//
//  main.m
//  ParseExample
//
//  Created by gstudio on 08.07.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
