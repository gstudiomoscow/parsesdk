//
//  AppDelegate.h
//  ParseExample
//
//  Created by gstudio on 08.07.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

