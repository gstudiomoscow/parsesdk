//
//  GSParseService.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseLogin.h"
#import "GSParseData.h"
#import "GSParseUserData.h"
#import "GSParsePush.h"

#import "GSParseUser.h"
#import "GSParseError.h"

@interface GSParseService : NSObject

@property (nonatomic, strong, readonly) GSParseLogin *parseLogin;
@property (nonatomic, strong, readonly) GSParseData *parseData;
@property (nonatomic, strong, readonly) GSParseUserData *parseUserData;
@property (nonatomic, strong, readonly) GSParsePush *parsePush;

- (void)initApp:(NSString *)appId serverUrl:(NSString *)serverUrl;

@end
