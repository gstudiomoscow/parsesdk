//
//  GSParseHelper.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseHelper.h"

#import "GSParseUser.h"

@implementation GSParseHelper

+ (GSParseHelper *)sharedInstance
{
    static id parseHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        parseHelper = [GSParseHelper new];
    });
    return parseHelper;
}

@end
