//
//  GSParseHelper.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@class GSParseUser;

@interface GSParseHelper : NSObject

@property (nonatomic, strong) NSString *appId;
@property (nonatomic, strong) NSString *serverUrl;
@property (nonatomic, strong) GSParseUser *currentUser;

+ (GSParseHelper *)sharedInstance;

@end
