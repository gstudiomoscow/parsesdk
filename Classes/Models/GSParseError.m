//
//  GSParseError.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseError.h"

static NSString *const GSErrorDataKey = @"com.alamofire.serialization.response.error.data";
static NSString *const GSStreamErrorKey = @"_kCFStreamErrorCodeKey";
static NSString *const GSStreamDescriptionKey = @"NSLocalizedDescription";

@implementation GSParseError

+ (instancetype)initWithError:(NSError *)error
{
    GSParseError *model = [GSParseError new];
    NSData *data = error.userInfo[GSErrorDataKey];
    if (data)
    {
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        model.code = json[@"code"];
        model.message = [self _errorMessageWithCode:model.code message:json[@"error"]];
    }
    else
    {
        model.code = error.userInfo[GSStreamErrorKey];
    }
    
    BOOL isNotConnectedToInternet = error.code == NSURLErrorNotConnectedToInternet;
    BOOL isTimedOut = error.code == NSURLErrorTimedOut;
    BOOL isNetworkConnectionLost = error.code == NSURLErrorNetworkConnectionLost;

    model.isConnectionProblem = isNotConnectedToInternet || isTimedOut || isNetworkConnectionLost;
    return model;
}

+ (NSString *)_errorMessageWithCode:(NSNumber *)code message:(NSString *)message
{
    if ([code isEqualToNumber:@202])
    {
        return @"Пользователь с таким номером уже зарегистрирован";
    }
    return message;
}

@end
