//
//  GSParseError.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@interface GSParseError : NSObject

@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, assign) BOOL isConnectionProblem;

+ (instancetype)initWithError:(NSError *)error;

@end
