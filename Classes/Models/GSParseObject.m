//
//  GSParseObject.m
//
//  Created by gstudio on 12.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseObject.h"

@implementation GSParseObject

- (NSString *)tableName
{
    NSAssert(false, @"Property table name not implement for object: '%@' !", [self class]);
    return nil;
}

@end
