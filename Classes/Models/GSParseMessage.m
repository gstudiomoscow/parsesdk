//
//  GSParseMessage.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseMessage.h"

static NSString *const GSKeyAps = @"aps";
static NSString *const GSKeyAlert = @"alert";
static NSString *const GSData = @"data";

@implementation GSParseMessage

+ (instancetype)initWithUserInfo:(NSDictionary *)userInfo application:(UIApplication *)application
{
    NSDictionary *aps = userInfo[GSKeyAps];
    GSParseMessage *model = [GSParseMessage new];
    model.message = aps[GSKeyAlert];
    model.headers = userInfo[GSData];
    model.state = application.applicationState == UIApplicationStateActive? GSParsePushStateActive : GSParsePushStateBackground;
    return model;
}

@end
