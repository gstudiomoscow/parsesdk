//
//  GSParseObject.h
//
//  Created by gstudio on 12.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@interface GSParseObject : NSObject

@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *updatedAt;

// required
@property (nonatomic, strong, readonly) NSString *tableName;

@end
