//
//  GSParseUser.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseObject.h"

@interface GSParseUser : GSParseObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *sessionToken;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, assign) BOOL isBlocked;

@end
