//
//  GSParseMessage.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

typedef NS_ENUM(NSInteger, GSParsePushState) {
    GSParsePushStateActive,
    GSParsePushStateBackground
};

@interface GSParseMessage : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSDictionary *headers;
@property (nonatomic, strong) NSArray *channels;
@property (nonatomic, assign) GSParsePushState state;

+ (instancetype)initWithUserInfo:(NSDictionary *)userInfo application:(UIApplication *)application;

@end
