//
//  GSParseService.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseService.h"
#import "GSParseHelper.h"
#import "GSParseNetwork.h"

@interface GSParseService ()

@property (nonatomic, strong) GSParseHelper *helper;
@property (nonatomic, strong) GSParseNetwork *network;
@property (nonatomic, strong, readwrite) GSParseLogin *parseLogin;
@property (nonatomic, strong, readwrite) GSParseData *parseData;
@property (nonatomic, strong, readwrite) GSParseUserData *parseUserData;
@property (nonatomic, strong, readwrite) GSParsePush *parsePush;

@end

@implementation GSParseService

- (void)initApp:(NSString *)appId serverUrl:(NSString *)serverUrl
{
    self.helper.appId = appId;
    self.helper.serverUrl = serverUrl;
}

#pragma mark - Lazy Initialization

- (GSParseHelper *)helper
{
    if (!_helper)
    {
        _helper = [GSParseHelper sharedInstance];
    }
    return _helper;
}

- (GSParseNetwork *)network
{
    if (!_network)
    {
        _network = [GSParseNetwork new];
    }
    return _network;
}

- (GSParseLogin *)parseLogin
{
    if (!_parseLogin)
    {
        _parseLogin = [GSParseLogin new];
        _parseLogin.networkService = self.network;
        _parseLogin.helper = self.helper;
    }
    return _parseLogin;
}

- (GSParseData *)parseData
{
    if (!_parseData)
    {
        _parseData = [GSParseData new];
        _parseData.networkService = self.network;
    }
    return _parseData;
}

- (GSParseUserData *)parseUserData
{
    if (!_parseUserData)
    {
        _parseUserData = [GSParseUserData new];
        _parseUserData.networkService = self.network;
    }
    return _parseUserData;
}

- (GSParsePush *)parsePush
{
    if (!_parsePush)
    {
        _parsePush = [GSParsePush new];
        _parsePush.networkService = self.network;
    }
    return _parsePush;
}

@end
