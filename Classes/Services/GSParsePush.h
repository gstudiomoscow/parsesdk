//
//  GSParsePush.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@class GSParseError;
@class GSParseNetwork;
@class GSParseMessage;
@protocol GSParsePushDelegate;

@interface GSParsePush : NSObject

@property (nonatomic, strong) GSParseNetwork *networkService;
@property (nonatomic, weak) id <GSParsePushDelegate> delegate;

// Methods for AppDelegate
// -----------------------
- (void)registerForRemoteNotifications;

- (void)openNotificationUserInfo:(NSDictionary *)userInfo application:(UIApplication *)application;

- (void)deviceRegistrationWithDeviceToken:(NSData *)deviceToken
                                completed:(void(^)())completed
                                  failure:(void(^)(GSParseError *error))failure;
// -----------------------

@end

@protocol GSParsePushDelegate <NSObject>

// If u want get messages delegate self
- (void)didReceiveParsePushMessage:(GSParseMessage *)parseMessage;

@end