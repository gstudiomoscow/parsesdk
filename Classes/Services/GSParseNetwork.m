//
//  GSParseNetwork.m
//

#import "GSParseNetwork.h"
#import "AFNetworking.h"
#import "GSParseHelper.h"

static NSString *const GSHeadersContentType = @"Content-Type";
static NSString *const GSHeadersApplicationId = @"X-Parse-Application-Id";
static NSString *const GSHeadersSessionToken = @"X-Parse-Session-Token";

@interface GSParseNetwork () <NSURLConnectionDelegate>

@property (nonatomic, weak) GSParseHelper *helper;

@end

@implementation GSParseNetwork

- (NSURLSessionDataTask *)GET:(NSString *)method contentType:(NSString *)contentType parameters:(NSDictionary *)parameters completed:(GSResponseBlock)completed failure:(GSErrorBlock)failure
{
    NSString *url = [self _urlWithParams:@[method]];
    AFHTTPSessionManager *manager = [self _sessionManagerWithContentType:contentType];
    
    NSURLSessionDataTask *task = [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [self _completed:completed withResponse:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self _failure:failure withError:error];
    }];
    return task;
}

- (NSURLSessionDataTask *)POST:(NSString *)method contentType:(NSString *)contentType parameters:(NSDictionary *)parameters completed:(GSResponseBlock)completed failure:(GSErrorBlock)failure
{
    NSString *url = [self _urlWithParams:@[method]];
    AFHTTPSessionManager *manager = [self _sessionManagerWithContentType:contentType];
    
    NSURLSessionDataTask *task = [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [self _completed:completed withResponse:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self _failure:failure withError:error];
    }];
    return task;
}

- (NSURLSessionDataTask *)DELETE:(NSString *)method contentType:(NSString *)contentType parameters:(NSDictionary *)parameters completed:(GSResponseBlock)completed failure:(GSErrorBlock)failure
{
    NSString *url = [self _urlWithParams:@[method]];
    AFHTTPSessionManager *manager = [self _sessionManagerWithContentType:contentType];
    
    NSURLSessionDataTask *task = [manager DELETE:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self _completed:completed withResponse:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self _failure:failure withError:error];
    }];
    return task;
}

- (NSURLSessionDataTask *)PUT:(NSString *)method contentType:(NSString *)contentType parameters:(NSDictionary *)parameters completed:(GSResponseBlock)completed failure:(GSErrorBlock)failure
{
    NSString *url = [self _urlWithParams:@[method]];
    AFHTTPSessionManager *manager = [self _sessionManagerWithContentType:contentType];
    
    NSURLSessionDataTask *task = [manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self _completed:completed withResponse:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self _failure:failure withError:error];
    }];
    return task;
}

- (NSURLSessionDataTask *)PUT:(NSString *)method contentType:(NSString *)contentType sessionToken:(NSString *)sessionToken parameters:(NSDictionary *)parameters completed:(GSResponseBlock)completed failure:(GSErrorBlock)failure
{
    NSString *url = [self _urlWithParams:@[method]];
    AFHTTPSessionManager *manager = [self _sessionManagerWithHeaders:@{GSHeadersContentType: contentType,
                                                                       GSHeadersSessionToken: sessionToken}];
    
    NSURLSessionDataTask *task = [manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self _completed:completed withResponse:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self _failure:failure withError:error];
    }];
    return task;
}

#pragma mark - Private

- (void)_progress:(GSProgressBlock)progress withFraction:(double)fraction;
{
    if (progress)
    {
        progress(fraction);
    }
}

- (void)_completed:(GSResponseBlock)completed withResponse:(NSError *)response;
{
    if (completed)
    {
        completed(response);
    }
}

- (void)_failure:(GSErrorBlock)failure withError:(NSError *)error;
{
    if (failure)
    {
        failure(error);
    }
}

- (NSString *)_urlWithParams:(NSArray<NSString *> *)params
{
    NSString *url = [NSString stringWithFormat:@"%@", [GSParseHelper sharedInstance].serverUrl];
    NSMutableString *mString = [NSMutableString stringWithString:url];
    [params enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [mString appendString:@"/"];
        [mString appendString:obj];
    }];
    return mString.copy;
}

- (AFHTTPSessionManager *)_sessionManagerWithContentType:(NSString *)contentType
{
    if (!contentType)
    {
        return [self _sessionManagerWithHeaders:nil];
    }
    return [self _sessionManagerWithHeaders:@{GSHeadersContentType: contentType}];
}

- (NSDictionary *)_headersWithContentType:(NSString *)contentType
{
    if (!contentType)
    {
        return [self _headersWithHeaders:nil];
    }
    return [self _headersWithHeaders:@{GSHeadersContentType: contentType}];
}

- (AFHTTPSessionManager *)_sessionManagerWithHeaders:(NSDictionary *)headers
{
    NSDictionary *mDict = [self _headersWithHeaders:headers];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if (mDict.count > 0)
    {
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [mDict enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
            [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
        }];
    }
    return manager;
}

- (NSDictionary *)_headersWithHeaders:(NSDictionary *)headers
{
    NSDictionary *defaultHeaders = [self _defaultHeaders];
    if (!headers)
    {
        return defaultHeaders;
    }
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithDictionary:defaultHeaders];
    [headers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        [mDict setObject:obj forKey:key];
    }];
    return mDict.copy;
}

- (NSDictionary *)_defaultHeaders
{
    return @{GSHeadersApplicationId: self.helper.appId};
}

#pragma mark - Lazy Initialization

- (GSParseHelper *)helper
{
    if (!_helper)
    {
        _helper = [GSParseHelper sharedInstance];
    }
    return _helper;
}

@end
