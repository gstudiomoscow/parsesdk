//
//  GSParseData.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@class GSParseError;
@class GSParseNetwork;
@class GSParseObject;

@interface GSParseData : NSObject

@property (nonatomic, strong) GSParseNetwork *networkService;

- (NSURLSessionDataTask *)uploadObject:(GSParseObject *)obj
                             completed:(void (^)(GSParseObject *obj))completed
                               failure:(void (^)(GSParseError *error))failure;

- (NSURLSessionDataTask *)uploadObjects:(NSArray *)objects
                                inTable:(NSString *)table
                              completed:(void (^)())completed
                                failure:(void (^)(GSParseError *))failure;

- (NSURLSessionDataTask *)downloadObject:(NSString *)objectId
                               fromTable:(NSString *)table
                             objectClass:(Class)objClass
                               completed:(void(^)(GSParseObject *obj))completed
                                 failure:(void (^)(GSParseError *error))failure;

- (NSURLSessionDataTask *)downloadObjects:(Class)objClass
                                fromTable:(NSString *)table
                                   params:(NSDictionary *)params
                                completed:(void(^)(NSArray *collection))completed
                                  failure:(void(^)(GSParseError *error))failure;

- (NSURLSessionDataTask *)deleteObject:(GSParseObject *)obj
                             completed:(void (^)(GSParseObject *obj))completed
                               failure:(void (^)(GSParseError *error))failure;

- (NSURLSessionDataTask *)updateObject:(GSParseObject *)obj
                                params:(NSDictionary *)params
                             completed:(void (^)(GSParseObject *obj))completed
                               failure:(void (^)(GSParseError *error))failure;

- (NSURLSessionDataTask *)postFunction:(NSString *)function
                                params:(NSDictionary *)params
                             completed:(void (^)(id result))completed
                               failure:(void (^)(GSParseError *error))failure;

@end
