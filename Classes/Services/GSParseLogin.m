//
//  GSParseLogin.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseLogin.h"

#import "GSParseNetwork.h"
#import "GSParseHelper.h"
#import "YYModel.h"

#import "GSParseUser.h"
#import "GSParseError.h"

static NSString *const GSMethodRegistration = @"users";
static NSString *const GSMethodLogin = @"login";
static NSString *const GSContentType = @"application/json";

static NSString *const GSHeaderKeyUsername = @"username";
static NSString *const GSHeaderKeyPassword = @"password";

static NSString *const GSPropertyIsBlock = @"isBlocked";

@implementation GSParseLogin

- (void)registrationUser:(GSParseUser *)user completed:(void (^)(GSParseUser *))completed failure:(void (^)(GSParseError *))failure
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:[user yy_modelToJSONObject]];
    [params removeObjectForKey:GSPropertyIsBlock];
    [self.networkService POST:GSMethodRegistration contentType:GSContentType parameters:params completed:^(id response) {
        GSParseUser *model = [GSParseUser yy_modelWithJSON:response];
        user.createdAt = model.createdAt;
        user.sessionToken = model.sessionToken;
        user.objectId = model.objectId;
        self.helper.currentUser = user;
        if (completed)
        {
            completed(user);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
}

- (void)loginWithClass:(Class)userClass userName:(NSString *)userName password:(NSString *)password completed:(void (^)(GSParseUser *user))completed failure:(void (^)(GSParseError *error))failure
{
    NSString *url = [self _urlWithMethod:GSMethodLogin params:@{GSHeaderKeyUsername: userName,
                                                                GSHeaderKeyPassword: password}];
    [self.networkService GET:url contentType:nil parameters:nil completed:^(id response) {
        GSParseUser *user = [userClass yy_modelWithJSON:response];
        self.helper.currentUser = user;
        if (completed)
        {
            completed(user);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
}

- (NSString *)_urlWithMethod:(NSString *_Nonnull)method params:(NSDictionary *)params
{
    NSMutableString *mString = [NSMutableString stringWithString:method];
    [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([mString isEqualToString:method])
        {
            [mString appendString:@"?"];
        }
        else
        {
            [mString appendString:@"&"];
        }
        [mString appendFormat:@"%@=%@", key, obj];
    }];
    return mString.copy;
}

@end
