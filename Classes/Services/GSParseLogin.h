//
//  GSParseLogin.h
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@class GSParseUser;
@class GSParseError;
@class GSParseNetwork;
@class GSParseHelper;
@protocol GSParseLoginDelegate;

@interface GSParseLogin : NSObject

@property (nonatomic, strong) GSParseNetwork *networkService;
@property (nonatomic, strong) GSParseHelper *helper;
@property (nonatomic, weak) id <GSParseLoginDelegate> delegate;

- (void)registrationUser:(GSParseUser *)user
               completed:(void (^)(GSParseUser *user))completed
                 failure:(void (^)(GSParseError *error))failure;

- (void)loginWithClass:(Class)userClass
              userName:(NSString *)userName
              password:(NSString *)password
             completed:(void (^)(GSParseUser *user))completed
               failure:(void (^)(GSParseError *error))failure;

@end

@protocol GSParseLoginDelegate <NSObject>


@end
