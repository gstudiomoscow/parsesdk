//
//  GSParsePush.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParsePush.h"
#import "GSParseNetwork.h"
#import "GSParseHelper.h"
#import "GSParseService.h"

#import "GSParseUser.h"
#import "GSParseMessage.h"
#import "GSParseError.h"

#import <UserNotifications/UserNotifications.h>
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static NSString *const GSMethodRegistration = @"installations";
static NSString *const GSMethodPush = @"push";
static NSString *const GSContentType = @"application/json";
static NSString *const GSPushNotification = @"GSPushNotification";

static NSString *const GSPropertyCurrentUser = @"currentUser";
static NSString *const GSKeyDeviceType = @"deviceType";
static NSString *const GSKeyDeviceToken = @"deviceToken";
static NSString *const GSKeyChannels = @"channels";
static NSString *const GSDeviceType = @"ios";

@interface GSParsePush () <UNUserNotificationCenterDelegate>

@property (nonatomic, strong) GSParseUserData *parseUserData;
@property (nonatomic, strong) NSString *deviceToken;

@end

@implementation GSParsePush

- (void)registerForRemoteNotifications
{
    [self _observeForUser];
    // test ios 10
    if (SYSTEM_VERSION_GRATERTHAN_OR_EQUAL_TO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error)
            {
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

- (void)deviceRegistrationWithDeviceToken:(NSData *)deviceToken completed:(void (^)())completed failure:(void (^)(GSParseError *))failure
{
    self.deviceToken = [self _deviceTokenFromData:deviceToken];
    [self _registerInstallationCompleted:completed failure:failure];
    [self _addDeviceTokenForUser];
}

- (void)openNotificationUserInfo:(NSDictionary *)userInfo application:(UIApplication *)application
{
    GSParseMessage *message = [GSParseMessage initWithUserInfo:userInfo application:application];
    [[NSNotificationCenter defaultCenter] postNotificationName:GSPushNotification object:message];
    #ifdef DEBUG
        NSLog(@"open push - '%@'", message.message);
    #endif
}

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setupInitialState];
    }
    return self;
}

- (void)setupInitialState
{
    [self _setupNotification];
}

#pragma mark - <UNUserNotificationCenterDelegate>

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    [self openNotificationUserInfo:userInfo application:[UIApplication sharedApplication]];
    completionHandler(UNNotificationPresentationOptionBadge);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    [self openNotificationUserInfo:userInfo application:[UIApplication sharedApplication]];
    completionHandler();
}

#pragma mark - KVO

- (void)_observeForUser
{
    GSParseHelper *parseHelper = [GSParseHelper sharedInstance];
    [parseHelper addObserver:self forKeyPath:GSPropertyCurrentUser options:NSKeyValueObservingOptionNew context:nil];
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:GSPropertyCurrentUser])
    {
        [self _addDeviceTokenForUser];
    }
}

- (void)_setupNotification
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(_messageDidOpen:) name:GSPushNotification object:nil];
}

- (void)_messageDidOpen:(NSNotification *)notification
{
    GSParseMessage *message = notification.object;
    if (message && [self.delegate respondsToSelector:@selector(didReceiveParsePushMessage:)])
    {
        [self.delegate didReceiveParsePushMessage:message];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private

- (void)_registerInstallationCompleted:(void (^)())completed failure:(void (^)(GSParseError *))failure
{
    NSDictionary *params = @{GSKeyDeviceType: GSDeviceType,
                             GSKeyDeviceToken: self.deviceToken,
                             GSKeyChannels: @[@""]};
    
    [self.networkService POST:GSMethodRegistration contentType:GSContentType parameters:params completed:^(id response) {
        if (completed)
        {
            completed();
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
}

- (void)_addDeviceTokenForUser
{
    GSParseUser *parseUser = [GSParseHelper sharedInstance].currentUser;
    
    if (!parseUser || !self.deviceToken)
    {
        return;
    }
    if (![parseUser.deviceToken isEqualToString:self.deviceToken])
    {
        NSDictionary *userParams = @{GSKeyDeviceToken: self.deviceToken};
        [self.parseUserData updateUser:parseUser.objectId sessionToken:parseUser.sessionToken params:userParams completed:nil failure:^(GSParseError *error) {
            #ifdef DEBUG
                NSLog(@"New device token failure save");
            #endif
        }];
    }
}

- (NSString *)_deviceTokenFromData:(NSData *)data
{
    NSString *value = [data description];
    value = [value stringByReplacingOccurrencesOfString:@"<" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@">" withString:@""];
    value = [value stringByReplacingOccurrencesOfString:@" " withString:@""];
    return value;
}

#pragma mark - Lazy Initialization

- (GSParseUserData *)parseUserData
{
    if (!_parseUserData)
    {
        _parseUserData = [GSParseService new].parseUserData;
    }
    return _parseUserData;
}

@end
