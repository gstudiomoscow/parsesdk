//
//  GSParseNetwork.h
//

typedef void(^GSResponseBlock)(id response);
typedef void(^GSProgressBlock)(double fractionCompleted);
typedef void(^GSErrorBlock)(NSError *error);

@interface GSParseNetwork : NSObject

- (NSURLSessionDataTask *)GET:(NSString *)method
                  contentType:(NSString *)contentType
                   parameters:(NSDictionary *)parameters
                    completed:(GSResponseBlock)completed
                      failure:(GSErrorBlock)failure;

- (NSURLSessionDataTask *)POST:(NSString *)method
                   contentType:(NSString *)contentType
                    parameters:(NSDictionary *)parameters
                     completed:(GSResponseBlock)completed
                       failure:(GSErrorBlock)failure;

- (NSURLSessionDataTask *)DELETE:(NSString *)method
                     contentType:(NSString *)contentType
                      parameters:(NSDictionary *)parameters
                       completed:(GSResponseBlock)completed
                         failure:(GSErrorBlock)failure;

- (NSURLSessionDataTask *)PUT:(NSString *)method
                  contentType:(NSString *)contentType
                   parameters:(NSDictionary *)parameters
                    completed:(GSResponseBlock)completed
                      failure:(GSErrorBlock)failure;

- (NSURLSessionDataTask *)PUT:(NSString *)method
                  contentType:(NSString *)contentType
                 sessionToken:(NSString *)sessionToken
                   parameters:(NSDictionary *)parameters
                    completed:(GSResponseBlock)completed
                      failure:(GSErrorBlock)failure;

@end
