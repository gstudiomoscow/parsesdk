
//  GSParseData.m
//
//  Created by gstudio on 11.05.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseData.h"
#import "GSParseNetwork.h"

#import "YYModel.h"
#import "GSParseError.h"
#import "GSParseObject.h"

static NSString *const GSMethodData = @"classes/";
static NSString *const GSMethodFunctions = @"functions/";
static NSString *const GSKeyResults = @"results";
static NSString *const GSKeyResult = @"result";
static NSString *const GSKeyObjectId = @"objectId";
static NSString *const GSContentType = @"application/json";
static NSString *const GSMethodBatch = @"batch";
static NSString *const GSBatchPath = @"/parse/classes/";
static NSString *const GSBatchRequests = @"requests";

static NSString *const GSKeyMethod = @"method";
static NSString *const GSKeyPath = @"path";
static NSString *const GSKeyBody = @"body";
static NSString *const GSPOST = @"POST";

@implementation GSParseData

- (NSURLSessionDataTask *)uploadObject:(GSParseObject *)obj completed:(void (^)(GSParseObject *obj))completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@", GSMethodData, obj.tableName];
    NSURLSessionDataTask *task = [self.networkService POST:method contentType:GSContentType parameters:[obj yy_modelToJSONObject] completed:^(id response) {
        if ([response isKindOfClass:[NSDictionary class]] && completed)
        {
            obj.objectId = response[GSKeyObjectId];
            completed(obj);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)uploadObjects:(NSArray *)objects inTable:(NSString *)table completed:(void (^)())completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@", GSMethodBatch];
    NSString *path = [NSString stringWithFormat:@"%@%@", GSBatchPath, table];
    NSMutableArray *mArray = [NSMutableArray new];
    [objects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *request = @{GSKeyMethod: GSPOST,
                                  GSKeyPath: path,
                                  GSKeyBody: obj};
        [mArray addObject:request];
    }];
    NSDictionary *params = @{GSBatchRequests: mArray.copy};
    
    NSURLSessionDataTask *task = [self.networkService POST:method contentType:GSContentType parameters:params completed:^(id response) {
        if (completed)
        {
            completed();
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)downloadObject:(NSString *)objectId fromTable:(NSString *)table objectClass:(Class)objClass completed:(void(^)(GSParseObject *obj))completed failure:(void (^)(GSParseError *error))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@/%@", GSMethodData, table, objectId];
    NSURLSessionDataTask *task = [self.networkService GET:method contentType:GSContentType parameters:nil completed:^(id response) {
        if (completed)
        {
            completed([objClass yy_modelWithJSON:response]);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)downloadObjects:(Class)objClass fromTable:(NSString *)table params:(NSDictionary *)params completed:(void(^)(NSArray *collection))completed failure:(void (^)(GSParseError *error))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@", GSMethodData, table];
    NSURLSessionDataTask *task = [self.networkService GET:method contentType:GSContentType parameters:params completed:^(id response) {
        NSArray *result = response[GSKeyResults];
        NSArray *collection = [self _collectionFromClass:objClass fromArray:result];
        if (completed)
        {
            completed(collection);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)deleteObject:(GSParseObject *)obj completed:(void (^)(GSParseObject *))completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@/%@", GSMethodData, obj.tableName, obj.objectId];
    NSURLSessionDataTask *task = [self.networkService DELETE:method contentType:nil parameters:nil completed:^(id response) {
        if (completed)
        {
            completed(obj);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)updateObject:(GSParseObject *)obj params:(NSDictionary *)params completed:(void (^)(GSParseObject *))completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@/%@", GSMethodData, obj.tableName, obj.objectId];
    NSURLSessionDataTask *task = [self.networkService PUT:method contentType:GSContentType parameters:params completed:^(id response) {
        if (completed)
        {
            completed(obj);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSURLSessionDataTask *)postFunction:(NSString *)function params:(NSDictionary *)params completed:(void (^)(id result))completed failure:(void (^)(GSParseError *error))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@", GSMethodFunctions, function];
    NSURLSessionDataTask *task = [self.networkService POST:method contentType:@"application/json" parameters:params completed:^(id response) {
        if (completed && [response isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *json = response;
            completed(json[GSKeyResult]);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
    return task;
}

- (NSArray *)_collectionFromClass:(Class)objClass fromArray:(NSArray *)array
{
    NSAssert(objClass != nil, @"Class must be not nill");
    NSMutableArray *mArray = [NSMutableArray new];
    for (id obj in array)
    {
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *json = obj;
            [mArray addObject:[objClass yy_modelWithJSON:json]];
        }
    }
    return mArray.copy;
}

@end
