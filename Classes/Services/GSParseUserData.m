//
//  GSParseUserData.m
//
//  Created by gstudio on 15.06.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

#import "GSParseUserData.h"
#import "GSParseNetwork.h"

#import "YYModel.h"
#import "GSParseError.h"
#import "GSParseUser.h"

static NSString *const GSUsersTable = @"users/";
static NSString *const GSContentType = @"application/json";

@implementation GSParseUserData

- (void)downloadUser:(NSString *)userId userClass:(Class)userClass completed:(void (^)(GSParseUser *))completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@", GSUsersTable, userId];
    [self.networkService GET:method contentType:nil parameters:nil completed:^(id response) {
        if ([response isKindOfClass:[NSDictionary class]] && completed)
        {
            completed([userClass yy_modelWithJSON:response]);
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
}

- (void)updateUser:(NSString *)userId sessionToken:(NSString *)sessionToken params:(NSDictionary *)params completed:(void (^)())completed failure:(void (^)(GSParseError *))failure
{
    NSString *method = [NSString stringWithFormat:@"%@%@", GSUsersTable, userId];
    [self.networkService PUT:method contentType:GSContentType sessionToken:sessionToken parameters:params completed:^(id response) {
        if (completed)
        {
            completed();
        }
    } failure:^(NSError *error) {
        if (failure)
        {
            failure([GSParseError initWithError:error]);
        }
    }];
}

@end
