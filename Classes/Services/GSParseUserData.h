//
//  GSParseUserData.h
//
//  Created by gstudio on 15.06.16.
//  Copyright © 2016 gstudio. All rights reserved.
//

@class GSParseError;
@class GSParseNetwork;
@class GSParseUser;

@interface GSParseUserData : NSObject

@property (nonatomic, strong) GSParseNetwork *networkService;

- (void)downloadUser:(NSString *)userId
           userClass:(Class)userClass
           completed:(void (^)(GSParseUser *user))completed
             failure:(void (^)(GSParseError *error))failure;

- (void)updateUser:(NSString *)userId
      sessionToken:(NSString *)sessionToken
            params:(NSDictionary *)params
         completed:(void (^)())completed
           failure:(void (^)(GSParseError *))failure;

@end
