
Pod::Spec.new do |s|

  s.name         = "ParseSDK"
  s.version      = "1.2.4"
  s.summary      = "Parse custom sdk library"

  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.homepage     = "https://bitbucket.org/gstudiomoscow/parsesdk"

  s.author             = { "Gstudio" => "alexandr@gstudio.com" }
  s.platform     = :ios, "7.0"
  
  s.source       = { :git => "https://alexandrgstudio@bitbucket.org/gstudiomoscow/parsesdk.git", :tag => "1.2.4" }

  s.source_files  = "Classes/*.{h,m}"
  s.source_files  = "Classes/**/*.{h,m}"
  s.source_files  = "Classes/**/**/*.{h,m}"
  s.source_files  = "Classes/**/**/**/*.{h,m}"

  s.public_header_files = "Classes/*.h"
  s.public_header_files = "Classes/**/*.h"
  s.public_header_files = "Classes/**/**/*.h"
  s.public_header_files = "Classes/**/**/*.h"

  s.framework       = 'Foundation', 'UIKit'
  s.requires_arc    = true

  s.dependency 'AFNetworking'
  s.dependency 'YYModel'

end
